package com.osvaldo.dataBase;


import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.*;

import javax.sql.DataSource;
import java.sql.Connection;

public class DB
{
    private static final Logger VMobjDepuration = LogManager.getLogger(DB.class);
    private String ip;
    private String port;
    private String usr;
    private String pass;
    private String base;
    private DataSource dataSource;
    private static DB dataBase;


    private DB()
    {
        ip = "DESKTOP-MP3QS7O";
        port = "1433";
        usr = "system";
        pass = "system";
        base = "replicatest";

        String url = "jdbc:sqlserver://" + ip + ":" + port + ";databaseName=" + base;
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        basicDataSource.setUsername(usr);
        basicDataSource.setPassword(pass);
        basicDataSource.setUrl(url);
        basicDataSource.setMaxIdle(150);
        basicDataSource.setMaxActive(150);
        dataSource = basicDataSource;
    }

    public static DB getInstance()
    {
        if (dataBase == null)
        {
            dataBase = new DB();
        }
        return dataBase;
    }

    public synchronized Connection getConnection()
    {
        try
        {
            return dataSource.getConnection();
        }
        catch (Exception e)
        {
            return null;
        }
    }


}
