package com.osvaldo;

import com.mchange.v2.async.RunnableQueue;

import org.apache.logging.log4j.*;

import java.util.concurrent.*;
import java.util.*;

public class processController
{
    private static final Logger depuracion = LogManager.getLogger(processController.class);
    private LlenadoCola VMobjLLenandoCola;


    public processController()
    {
        VMobjLLenandoCola = LlenadoCola.getInstance();
    }

    public void process()
    {
        int corePoolSize = 50;
        int maxPoolSixze = 150;
        long keepAliveTime = 5000;
        boolean cambio = false;
        long tolerancia = System.currentTimeMillis() + 1000;
        String pausa = "";


        ExecutorService executor = new ThreadPoolExecutor(corePoolSize, maxPoolSixze, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        //ExecutorService executor = Executors.newCachedThreadPool();
        depuracion.info("Inica procesamiento");
        while (true)
        {

            if (VMobjLLenandoCola.getTamañoCola() > 0 && VMobjLLenandoCola.getTamañoCola() < 100)
            {
                if (executor.isTerminated())
                {
                    executor = new ThreadPoolExecutor(corePoolSize, maxPoolSixze, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
                }
                System.out.println(VMobjLLenandoCola.getTamañoCola());
                try
                {
                    String dato = VMobjLLenandoCola.getData();
                    if (dato != null)
                    {
                        Runnable worker = new Hilo(dato);
                        executor.execute(worker);
                        if (cambio)
                        {
                            ((ThreadPoolExecutor) executor).setCorePoolSize(50);
                            cambio = false;
                        }
                    }
                    else
                    {
                        executor.shutdown();
                        System.out.println("Cola vacia");
                        while (!executor.isTerminated())
                        {
                            Thread.sleep(1000);
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if (VMobjLLenandoCola.getTamañoCola() >= 100)
            {
                System.out.println("creciendo pool");
                if (executor.isTerminated())
                {
                    executor = new ThreadPoolExecutor(corePoolSize, 150, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
                }

                if (!cambio)
                {
                    ((ThreadPoolExecutor) executor).setCorePoolSize(100);
                }
                cambio = true;

                //((ThreadPoolExecutor) executor).setCorePoolSize(100);

                System.out.println(VMobjLLenandoCola.getTamañoCola());
                try
                {
                    String dato = VMobjLLenandoCola.getData();
                    if (dato != null)
                    {
                        Runnable worker = new Hilo(dato);
                        executor.execute(worker);
                    }
                    else
                    {
                        System.out.println("Cola vacia");
                        executor.shutdown();
                        while (!executor.isTerminated())
                        {
                            Thread.sleep(1000);
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                /*try
                {
                    Thread.sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }*/
            }
            else if (VMobjLLenandoCola.getTamañoCola() == 0)
            {

                executor.shutdown();
                if (tolerancia <= System.currentTimeMillis())
                {

                    pausa = "Se supone se vacio la cola, tamaño: " + VMobjLLenandoCola.getTamañoCola();
                    System.out.println(pausa);
                    depuracion.info(pausa);
                    tolerancia = System.currentTimeMillis() + (1 * 1000);
                    //pausa = "Se supone se vacio la cola, tamaño: " + VMobjLLenandoCola.getTamañoCola();
                }
                try
                {
                    while (!executor.isTerminated())
                    {
                        Thread.sleep(5000);
                    }
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }

    }


}
