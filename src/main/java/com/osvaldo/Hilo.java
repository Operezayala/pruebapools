package com.osvaldo;

import com.osvaldo.dataBase.DB;
import org.apache.logging.log4j.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Hilo implements Runnable
{
    public static final Logger VMobjDepuracion = LogManager.getLogger(Hilo.class);
    private DB dataBase;
    private Connection conexion;
    private String descripcion;
    private String boleano;


    public Hilo(String data)
    {
        String[] arreglo = data.split("\\|\\|");
        descripcion = arreglo[0];
        boleano = arreglo[1];
        dataBase = DB.getInstance();
    }

    public void run()
    {
        Statement declaration = null;
        String query = "insert into prueba1 (descripcion,estatus, creado) values('" + descripcion + "','" + boleano + "', getdate())";
        try
        {
            conexion = dataBase.getConnection();
            declaration = conexion.createStatement();
            declaration.executeUpdate(query);
            declaration.close();
        }
        catch (Exception e)
        {
            if (declaration != null)
            {
                try
                {
                    declaration.close();
                }
                catch (SQLException e1)
                {
                    e1.printStackTrace();
                }
            }
            VMobjDepuracion.error("Error: " + e.getMessage());
        }
        finally
        {
            try
            {
                conexion.close();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }


}
