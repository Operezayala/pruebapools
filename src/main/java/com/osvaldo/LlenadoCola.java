package com.osvaldo;


import org.apache.logging.log4j.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class LlenadoCola
{
    private final static Logger depuracion = LogManager.getLogger(LlenadoCola.class);
    private static LlenadoCola VMobjLlenandoCola;
    private Queue<String> colaMuestra;
    private Thread VMthdLlena;

    private LlenadoCola()
    {
        //colaMuestra = new LinkedList<String>();
        colaMuestra = new ConcurrentLinkedQueue<String>();
    }

    public void putData(String data)
    {
        colaMuestra.add(data);
    }

    public static synchronized LlenadoCola getInstance()
    {
        if (VMobjLlenandoCola == null)
        {
            VMobjLlenandoCola = new LlenadoCola();
        }
        return VMobjLlenandoCola;
    }

    public  int getTamañoCola()
    {
        if (colaMuestra != null)
        {
            synchronized (colaMuestra)
            {
                return colaMuestra.size();
            }
        }
        else
        {
            return 0;
        }
    }

    public synchronized String getData()
    {
        synchronized (colaMuestra)
        {
            return colaMuestra.poll();
        }
    }

    public void encolando(final int min, final int max)
    {
        try
        {
            if (VMthdLlena == null)
            {
                VMthdLlena = new Thread();
            }

            if (!VMthdLlena.isAlive())
            {
                VMthdLlena = new Thread()
                {
                    public void run()
                    {
                        long tiempo = System.currentTimeMillis() + (30 * 1000);
                        int counter = 0;
                        System.out.println("Inicia llenado");
                        while (true)
                        {
                            int aleatorioLlenado = (int) Math.floor(Math.random() * (min - (max + min)) + (max + min));
                            int aleatorioEspera = (int) Math.floor(Math.random() * (1 - (5 + 1)) + (5 + 1));

                            if (tiempo > System.currentTimeMillis())
                            {
                                counter++;

                                if ((counter % 2) == 0)
                                {
                                    putData("hola" + (counter) + "||true");
                                }
                                else
                                {
                                    putData("adios" + (counter) + "||false");
                                }
                                try
                                {
                                    //System.out.println("Tiempo entre trx: " + aleatorioLlenado * 10 + " ms");
                                    Thread.sleep(aleatorioLlenado);
                                }
                                catch (InterruptedException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                            else
                            {
                                //System.out.println("pasaron 30 segundos: " + spf.format(new Date()));
                                System.out.println("Registros encolados: " + counter);
                                depuracion.info("Registros encolados: " + counter);
                                try
                                {
                                    depuracion.info("Tiempo sin recibir trx: " + aleatorioEspera + " s");
                                    Thread.sleep(aleatorioEspera * 100);
                                }
                                catch (InterruptedException e)
                                {
                                    e.printStackTrace();
                                }
                                tiempo = System.currentTimeMillis() + (30 * 1000);
                            }
                        }
                    }
                };
                VMthdLlena.setDaemon(false);
                VMthdLlena.start();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
