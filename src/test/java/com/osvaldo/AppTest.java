package com.osvaldo;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.math.*;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
{

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName)
    {
        // super(testName);
        
        double _partida = 4.36;
        double _cantidad = 10;
        double total = _partida * _cantidad;

        System.out.println(total);

        BigDecimal partida = new BigDecimal("4.36");
        BigDecimal cantidad = new BigDecimal(15);
        //partida.setScale(2);
        partida = partida.multiply(cantidad);

        System.out.println(partida);


        BigDecimal subtotal;


        BigDecimal montoTotal = new BigDecimal("1525.59");
        BigDecimal pagado = new BigDecimal("-1000");

        BigDecimal adeudo = montoTotal.add(pagado);
        System.out.println("El adeudo es " + adeudo);


    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue(true);
    }
}
